package com.ism.services;


import com.ism.dto.FlightDto;
import com.ism.mapper.FlightMapper;
import com.ism.model.Flight;
import com.ism.repository.FlightRepository;
import com.ism.service.FlightService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FlightServiceTest {

    @Mock
    private FlightRepository flightRepository;

    @InjectMocks
    private FlightService flightService;


    @Test
    public void addFlght(){
        Flight flight = Flight.builder().flightNumber("777").destinationAirportCode("AXT").flightDate(new Date()).build();

        when(flightRepository.save(Mockito.any(Flight.class))).thenReturn(flight);

        Flight savedFlight = flightService.addFlght(flight);

        Assertions.assertNotNull(savedFlight);

    }
}
