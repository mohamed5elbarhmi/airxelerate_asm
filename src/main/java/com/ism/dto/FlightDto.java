package com.ism.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FlightDto implements Serializable {

    private Long id;
    private String carrierCode;
    private String flightNumber;
    private Date flightDate;
    private String originAirportCode;
    private String destinationAirportCode;

}
