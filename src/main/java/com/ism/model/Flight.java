package com.ism.model;


import jakarta.persistence.*;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Builder
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "flight")
public class Flight{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name="carrierCode")
    private String carrierCode;

    @NotNull
    @Size(min = 4, max = 4, message = "Flight number must have exactly 4 char")
    @Column(name="flightNumber")
    private String flightNumber;

    @NotNull
    @Column(name="flightDate")
    private Date flightDate;

    @NotNull
    @Column(name="originAirportCode")
    private String originAirportCode;

    @NotNull
    @Column(name="destinationAirportCode")
    private String destinationAirportCode;


}
